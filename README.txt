----------------------------------------------------------------------------
                     D R U P A L    M O D U L E
----------------------------------------------------------------------------
Name: Webform Select Term
Author: Andrey Vitushkin <andrey.vitushkin at gmail.com>
Drupal: 7.x


INTRODUCTION
------------
This module extends 'Select options' webform component.
It enables to prepopulate the list with titles of taxonomy terms.
Also it enables to display a description of a selected taxonomy term.


REQUIREMENTS
------------
This module does not requires additional modules.


INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
For further information visit:
https://www.drupal.org/docs/7/extend/installing-modules


CONFIGURATION
-------------
The module has no menu or modifiable settings.
There is no configuration.


HOW TO USE
------------
1. Add 'Select options' webform component.
2. Select a taxonomy vocabulary from the 'Load a pre-built option list' listbox.
3. Set 'Display term description' option to display terms descriptions.
